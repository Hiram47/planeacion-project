import {Router} from 'express'
import {authRequired} from '../middlewares/validateToken.js'
import { createListPrevencion, createTask, deleteListPrevencion, deleteTask, getListTena, getTask, getTaskByFilters, gettasks, updateTask } from '../controllers/tasks.controller.js'

const router = Router()

router.get('/tasks',authRequired,gettasks)
router.get('/tasks/:id',authRequired,getTask)
router.post('/tasks',authRequired,createTask)
router.delete('/tasks/:id',authRequired,deleteTask)
router.put('/tasks/:id',authRequired,updateTask)

router.post('/tasks/filtro',authRequired,getTaskByFilters)
router.get('/temaPrevencion',authRequired,getListTena)
router.post('/temaPrevencion',authRequired,createListPrevencion)

router.delete('/temaPrevencion/:id',authRequired,deleteListPrevencion)


export default router