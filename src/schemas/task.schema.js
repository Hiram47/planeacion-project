import {z} from 'zod'

export const createTaskSchema= z.object({
    marca: z.string({
        required_error: 'Marca Must be a string'
    }),
    submarca: z.string({
        required_error: 'Submarca Must be a string'
    }),
    modelo: z.string({
        required_error: 'Modelo Must be a string'
    }),
    tipo: z.string({
        required_error: 'Tipo Must be a string'
    }),
    color: z.string({
        required_error: 'Color Must be a string'
    }),
    no_motor: z.string({
        required_error: 'No motor Must be a string'
    }),
    serie: z.string({
        required_error: 'Serie Must be a string'
    }),
    placas: z.string({
        required_error: 'Placa Must be a string'
    }),
    inventario: z.string({
        required_error: 'Inventario Must be a string'
    }),

})