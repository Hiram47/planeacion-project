import {z} from 'zod'

export const registerSchema=z.object({
    username:z.string({
        required_error: 'Username is required'
    }),
    email:z.string({
        required_error:'Email is required'
    }).email({message:'invalid email'}),
    password: z.string({
        required_error:'Password is required'
    }).min(3,{message:'Password must be at least 3 characters'}),
    // role:z.number({
    //     required_error:'is a number',
    //     invalid_type_error:'must be a number'
    // })

})

export const loginSchema = z.object({
    email: z.string({
        required_error:'Email is requiered'
    }).email({
        message:'Email is not valid'
    }),

    password: z.string({
        required_error:'Password is required'
    }).min(3,{
        message:'Password must be at least 3 characters'
    })
})