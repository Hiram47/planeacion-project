import mysql from 'mysql2/promise'
import {config} from './config.js'

export const connectDB = async () =>{
    try {
        
        console.log('db is running');
        return await mysql.createConnection(config)
        
    } catch (error) {
        console.log(error);
        console.log('Error en conectar db');
        
    }
}