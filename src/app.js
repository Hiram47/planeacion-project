import express from 'express'
import morgan from 'morgan'
import cookieParser from 'cookie-parser'
import cors from 'cors'

import authRoutes from './routes/auth.routes.js'
import tasksRoutes from './routes/task.routes.js'

const app= express()

app.use(cors({
    origin:['http://10.11.60.124:5173','http://10.11.60.124:9898'],
    credentials:true
}))
app.use(morgan('dev'))
app.use(express.json())
app.use(cookieParser())

app.use('/api',authRoutes)
app.use('/api',tasksRoutes)


export default app;
// app.listen(3000)
// console.log('Server on port',3000);