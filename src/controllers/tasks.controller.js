import {connectDB} from '../db.js'


export const gettasks = async (req,res)=>{

    try {
        const db = await connectDB()
        const [result]=await db.query('SELECT * FROM `prevencion`;')

        res.json(result)
        
    } catch (error) {
        console.log(error);
        
    }


}

export const createTask = async (req,res)=>{

    try {
       const db= await connectDB()


       const result =await db.query('INSERT INTO prevencion set ?',[{...req.body,usuario:req.user.id}])

       console.log(result);
        console.log(req.body);

        res.status(200).json('Data saved successfuly')
        
    } catch (error) {

        console.log(error);

    }

    
}

export const getTask = async (req,res)=>{
    try {
        let id = req.params.id
        console.log(id);

        const db= await connectDB();
        const [result]=await db.query('SELECT * FROM prevencion where id= ?',[id])

        console.log(result[0]);

        res.json(result[0])





    } catch (error) {
        console.log(error);
        
    }

    
}
// searching by filters

export const getTaskByFilters = async (req,res)=>{
    try {
        let id = req.params.id
        console.log(id);
        console.log(req.body);
        const {fecha_inicial,fecha_final,capitulo,accion,municipio,relevante}=req.body

        const db= await connectDB();
        const [result]=await db.query('SELECT * FROM prevencion  WHERE fecha BETWEEN ? AND ? AND capitulo LIKE ? AND accion LIKE ? AND municipio LIKE ? AND relevante LIKE ? ',[fecha_inicial,fecha_final,`%${capitulo}%`,`%${accion}%`,`%${municipio}%`,`%${relevante}%`])

        console.log(result);

        res.json(result)





    } catch (error) {
        console.log(error);
        
    }

    
}


//en search

export const updateTask = async (req,res)=>{

    try {
        let id = req.params.id
        const db= await connectDB()

        const result=await db.query('UPDATE prevencion set ? where id=?',[req.body,id])
        console.log(result);

        res.status(201).json({message:'vehicle updated successfuly'})
        
    } catch (error) {
        console.log(error);
        res.status(500).json(error)
        
    }

    
}
export const deleteTask = async (req,res)=>{
    try {
        let id= req.params.id
        const db= await connectDB()
        const resp=await db.query('DELETE FROM prevencion where id=?',[id])
        console.log(resp);

        res.status(204).json({message:'Redcord was deleted succesfuly'})
        
    } catch (error) {
        console.log(error);
    }

    
}


// llenado de combobox
export const getListTena = async (req,res)=>{

    try {
        const db = await connectDB()
        const [result]=await db.query('SELECT * FROM `actividades_prevencion`;')

        res.json(result)
        
    } catch (error) {
        console.log(error);
        
    }


}
///
export const createListPrevencion = async (req,res)=>{

    try {
       const db= await connectDB()


       const result =await db.query('INSERT INTO actividades_prevencion set ?',[{...req.body,usuario:req.user.id}])

       console.log(result);
        console.log(req.body);

        res.status(200).json('Data saved successfuly')
        
    } catch (error) {

        console.log(error);

    }

    
}

export const deleteListPrevencion = async (req,res)=>{
    try {
        let id= req.params.id
        const db= await connectDB()
        const resp=await db.query('DELETE FROM actividades_prevencion where id=?',[id])
        console.log(resp);

        res.status(204).json({message:'Redcord was deleted succesfuly'})
        
    } catch (error) {
        console.log(error);
    }

    
}