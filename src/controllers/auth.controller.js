import {connectDB} from '../db.js';
import bcrypt from 'bcryptjs'
import jwt from 'jsonwebtoken'
import { TOKEN_SECRET } from '../config.js';

export const register= async(req,res)=>{
    //console.log(req.body);

    try {
        
        const {email,password,username,area,role} = req.body

        const db= await connectDB()
        const [userFound]=await db.query('SELECT * FROM usuarios WHERE email=? ',[email])
        console.log(userFound[0]);
   
        if (userFound[0]) return res.status(400).json(['El Email ya ha sido registrado'])


        const passwordhashed =await bcrypt.hash(password,10)
        console.log(email,password,username,area,role);

        await db.query('INSERT INTO `usuarios`(`username`, `email`, `password`, `role`, `area`) VALUES (?,?,?,?,?)',[username,email,passwordhashed,role,area])
        const [result]= await db.query('SELECT id,username,email from usuarios order by id desc limit 1')


        const token =jwt.sign({id:result[0].id},TOKEN_SECRET,{expiresIn:'1d'})
        console.log(token);
        res.cookie('token',token)




       // res.send('register')
       res.json(result[0])
    } catch (error) {
        console.log(error);
       // res.status(500).json({message: error.message})
        
    }


}

export const login= async(req,res)=>{
    const {email,password} = req.body;

    console.log(email,password);
    try {
        const db=await connectDB()
        
        const [res2] =await db.query('SELECT * from usuarios where email =?',[email])

        if (!res2[0]) {
            res.status(400).json(['Incorrect User'])
        }

        const isMatch=await bcrypt.compare(password,res2[0].password)

        if (!isMatch) {
            res.status(401).json(['Contrasena Incorrecta'])
        }

        const token =jwt.sign({id:res2[0].id},TOKEN_SECRET,{expiresIn:'1d'})

        console.log(token);

        res.cookie('token',token
        ,{
           sameSite:'lax'
         ,secure:false
       //   ,httpOnly:true
       }
       )

        res.send(res2[0])


    } catch (error) {
        console.log(error);
       // res.status(500).json({message: error.message})
        
    }



    
}

export const logout= (req,res)=>{
    res.cookie('token','',{
        expires: new Date(0)
    })

    return res.sendStatus(200)
    
}

export const profile = async(req,res)=>{
    try {        
        const db=await connectDB()
        const [res2] =await db.query('SELECT * from usuarios where id =?',[req.user.id])

        if (!res2[0]) {
            
            res.status(400).json({message:'User not found'})
            
        }

        console.log(req.user);
        res.json(res2[0])
    } catch (error) {

        console.log(error);
        
    }
}

export const verifyTokenReact = async(req,res)=>{
    const {token}=req.cookies
    if(!token) return res.status(401).json({message:'Unauthorized'})

    jwt.verify(token,TOKEN_SECRET,async(err,user)=>{
        if(err) return res.status(401).json({message:'Unauthorized'})
        try {
            const db=await connectDB()
            console.log(user.id);
        const [res2] =await db.query('SELECT * from usuarios where id =?',[user.id])
        if (!res2[0]) {
            
            res.status(400).json({message:'Unauthorized'})
            
        }
        return res.json(res2[0])
            
        } catch (error) {
            console.log(error);
            
        }
    })

}