FROM node:20-alpine3.19

WORKDIR /spring

COPY package.json .

RUN npm install

COPY /src* .

EXPOSE 3090

CMD [ "npm","run","dev" ]