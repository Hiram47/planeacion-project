#instructions to install

1 create Docker file an .dockerignore

    node_modules
    dist

2 add minstruction on Dockerfile

    FROM node:20.10.0-alpine3.19

    WORKDIR /app

    COPY package.json .

    RUN npm install

    COPY /public* .

    COPY /src* .

    COPY . .
    RUN npm run build 




    EXPOSE 9898

    CMD [ "npm","run","serve" ]

3 run commando on terminal

    docker build --tag 'planeacion-front' .

    docker run --name planeacion-f -p9898:5173 planeacion-front




