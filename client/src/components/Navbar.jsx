import React from 'react'
import { Link } from 'react-router-dom'
import { useAuth } from '../context/AuthContext'
import ssp from '../assets/ssp.png'

export const Navbar = () => {

    const {isAuthenticated,user,logout}=useAuth()
    console.log(user);
  return (
    <nav className='bg-zinc-700  flex justify-between py-5 px-10 '>
        <Link to={'/'}>
        
        <h1 className='text-3xl font-bold flex justify-between ' ><img style={{width:'38px'}} className='px-1' src={ssp} alt="SSP" /> SSP</h1>
        </Link>
        <ul className='flex gap-x-2'>
          {
            isAuthenticated ? (
            <>
                <li>
                <i className="fa fa-user-circle-o" aria-hidden="true"></i>
                    {user.username?.toUpperCase()}
                </li>
                <li>
                    <Link to={'/admin'} className='bg-sky-700 px-4 py-1 rounded-sm' >Administrador</Link>
                </li>
                <li>
                    <Link to={'/add-task'} className='bg-sky-700 px-4 py-1 rounded-sm' >Prevencion</Link>
                </li>
                <li>
                    <Link className='bg-red-800 py-1 px-3 rounded-sm' to={'/'} onClick={()=>{logout()}} ><i className="fa fa-sign-out" aria-hidden="true"></i>
Logout</Link>
                </li>


            </>):(
                <>
                <li>
                <Link to={'/login'} className='bg-sky-700 px-4 py-1 rounded-sm'><i className="fa fa-sign-in" aria-hidden="true"></i>
Login </Link>

            </li>
            {/* <li>
                <Link to={'/register'} className='bg-sky-700 px-4 py-1 rounded-sm'> <i className="fa fa-user-plus" aria-hidden="true"></i>
Registro </Link>

            </li> */}
                </>
            )
          }
        </ul>

    </nav>
  )
}
