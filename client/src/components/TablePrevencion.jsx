import React, { useEffect } from 'react'

import dayjs from 'dayjs'
import utc from 'dayjs/plugin/utc'
import { useForm } from 'react-hook-form'
import { useTasks } from '../context/TasksContext'
import {toast} from 'sonner'
import { Link } from 'react-router-dom'


dayjs.locale('es')
dayjs.extend(utc)

// const date = new Date();

// let day = date.getDate();
// let month = date.getMonth() + 1;
// let year = date.getFullYear();

// This arrangement can be altered based on how we want the date's format to appear.
// let currentDate=''
// if (month<10) {
//     currentDate = `${year}-0${month}-${day}`;
    
// }else{
//     currentDate = `${year}-${month}-${day}`;
    
// }
// console.log(currentDate); // "17-6-2022"


export const TablePrevencion = ({task,i,role,area}) => {
    const {register,handleSubmit,setValue}=useForm()
  
    // useEffect(() => {
    //     setValue('fecha_registro',currentDate)
    // }, [])
    
    

    const {deleteTask}=useTasks()

    const onDelete = async(id)=>{
        toast('Estas seguro de eliminar el elemento?',{
            action:{
                label:'Aceptar',
                onClick: async()=>{
                    const res=await deleteTask(id)
                      console.log(id);
                      console.log(res);
                    console.log(id);
                }
            },
            cancel: {
                label: 'Cancelar',
                onClick: () => console.log('Cancel!'),
              },
          })
    }

    
    
    
  return (
    <tr className=' border-b dark:bg-zinc-800 dark:border-gray-800 hover:bg-gray-500 dark:hover:bg-gray-600'
    // className='border-b dark:border-neutral-500' 
    >

    <td className='whitespace-nowrap  px-4 py-2 font-sans'>{i+1}</td>
    <td className='px-2 py-2'>{dayjs.utc(task.fecha ).locale('es').format('DD/MM/YYYY')}</td>
    <td className='px-2 py-2'>{task.capitulo.toUpperCase()}</td>
    <td className='px-2 py-2'>{task.componente.toUpperCase()}</td>
    <td className='px-2 py-2'>{task.actividad.toUpperCase()}</td>
    <td className='px-2 py-2'>{task.accion.toUpperCase()}</td>
    <td className='px-2 py-2'>{task.tema.toUpperCase()}</td>
    <td className='px-2 py-2'>{task.descripcion.toUpperCase()}</td>
    <td className='px-2 py-2'>{task.factor.toUpperCase()}</td>
    <td className='px-2 py-2'>{task.grupo.toUpperCase()}</td>
    <td className='px-2 py-2'>{task.mujeres}</td>
    <td className='px-2 py-2'>{task.hombres}</td>
    <td className='px-2 py-2'>{task.lugar.toUpperCase()}</td>
    <td className='px-2 py-2'>{task.municipio.toUpperCase()}</td>
    <td className='px-2 py-2'>{task.colonia.toUpperCase()}</td>
    <td className='px-2 py-2'>{task.latitud}</td>
    <td className='px-2 py-2'>{task.longitud}</td>
    <td className='px-2 py-2'>{task.alerta_genero.toUpperCase()}</td>
    <td className='px-2 py-2'>{task.estrategia_vive.toUpperCase()}</td>
    <td className='px-2 py-2'>{task.medio_verificacion.toUpperCase()}</td>


{/* new */}
    <td className='px-2 py-2'>{task.descripcion_accion.toUpperCase()}</td>
    <td className='px-2 py-2'>{task.acuerdos.toUpperCase()}</td>
    <td className='px-2 py-2'>{task.participantes}</td>
    <td className='px-2 py-2'>{task.observaciones.toUpperCase()}</td>
    {
      task.relevante ==1?
    <td className='px-2 py-2'>ACCION RELEVANTE</td>
    :
    <td className='px-2 py-2'></td>

    }

    {
        role===0  && area=='planeacion'&&
        <>
    <td className='px-2 py-2'><button className='bg-red-600 p-2 rounded-sm' onClick={()=>{onDelete(task.id)}}>Eliminar</button></td>
    <td className='px-2 py-2'><Link className='bg-yellow-500 p-2 rounded-sm' to={`/tasks/${task.id}`}>Editar</Link></td>
        </>
    }

      {/* <td className='px-2 py-2'>{gasto.motivo.toUpperCase()}</td>
      <td className='px-2 py-2'>{dayjs.utc(gasto.fecha ).locale('es').format('DD/MM/YYYY')}</td>
      <td className='px-2 py-2'>$ {new Intl.NumberFormat().format(gasto.cantidad)}</td>
      <td className='px-2 py-2'>$ {new Intl.NumberFormat().format(totald)}</td>
      <td  className='px-2 py-2 rounded-md  text-white  font-bold'><i style={{fontSize:'25px'}} className="fa fa-trash "  onClick={()=>ondelete(gasto.id)} ></i></td>
      
    */}
      {/* <td><button className='bg-red-800 hover:bg-red-600 p-1 m-2 rounded-md font-bold' onClick={()=>'hola'}>Eliminar</button></td> */}
      {/* <td  className='px-2 py-2'><Link className='bg-red-500 hover:bg-red-600 rounded-md min-w-full text-white  font-bold p-2' to={`/vehiculo/${gasto.id}`}><i style={{fontSize:'25px'}} className="fa fa-trash" ></i></Link></td> */}
    {/* <td className='px-2 py-2'>{dayjs.utc(task.fecha_registro ).locale('es').format('DD/MM/YYYY')}</td> */}

     
</tr>  
  )
}
