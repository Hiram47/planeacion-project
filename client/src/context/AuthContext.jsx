import { createContext, useContext, useEffect, useState } from "react";
import {registerRequest,loginRequest, verifyTokenRequest} from '../api/auth'
import Cookies from 'js-cookie'

export const AuthContext = createContext()

export const useAuth = () =>{
    const context = useContext(AuthContext);

    if(!context){
        throw new Error('useAuth must be used within an AuthProvider')
    }
    return context
}


export const AuthProvider = ({children})=>{

    const [user, setuser] = useState(null)
    const [isAuthenticated, setisAuthenticated] = useState(false)
    const [errors, seterrors] = useState([])
    const [loading, setloading] = useState(true)

    const signup= async(user) =>{

        try {
            
            const res= await registerRequest(user)
            console.log(res.data);
            console.log(user);
            setuser(res.data)
            setisAuthenticated(true)
        } catch (error) {
            console.log(error);
            console.log(error.response.data);
            seterrors(error.response.data)
            
        }

    }

    const signin = async(user)=>{
        try {
            const res=await loginRequest(user)
            console.log(res);
            setuser(res.data)
            setisAuthenticated(true)
            
        } catch (error) {
            console.log(error);
            seterrors(error.response.data)
            
        }
    }

    const logout = ()=>{
        Cookies.remove('token')
        setisAuthenticated(false)
        setuser(null)
    }



    useEffect(() => {
   if (errors.length >0) {
    const timer=setTimeout(() => {
        seterrors([])
        
    }, 5000);
    return () => clearTimeout(timer)
    
   }
    }, [errors])
    

    useEffect(() => {
        async function checkLogin(){

            const cookies=Cookies.get()
            console.log(cookies);
    
            if (!cookies.token) {
                setisAuthenticated(false)
                setloading(false)
                return setuser(null)
            }
            try {
                    console.log(cookies.token);
                    
                    const res=await verifyTokenRequest(cookies.token)
                    console.log(res.data);
    
                    if(!res.data) {

                        setisAuthenticated(false)
                        setloading(false)
                        return;
                    } 
    
                    setisAuthenticated(true)
                    setuser(res.data)
                    setloading(false)
    
                } catch (error) {
                    console.log(error);
                    setisAuthenticated(false)
                    setuser(null)
                    setloading(false)
                    
                }
                
        }
        checkLogin()
     
    }, [])
    


    return (
        <AuthContext.Provider value={{
            signup,
            user,
            isAuthenticated,
            errors,
            signin,
            loading,
            logout
        }}>
            {children}
        </AuthContext.Provider>
    )
}
