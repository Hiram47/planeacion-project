import { createContext, useContext, useState } from "react";
import { createListPrevencionRequest, createTaskRequest, deleteListPrevencionRequest, deleteTaskRequest, filterTaskRequest, getListPrevencionRequest, getTaskRequest, getTasksRequest, updateTaskRequest } from "../api/tasks";


const TaskContext=createContext()

export const useTasks=()=>{
    const context = useContext(TaskContext)

    if (!context) {

        throw new Error('usetasks must be used withn a TaskProvider')
        
    }

    return context
}



export function TaskProvider({children}){

    const [tasks, settasks] = useState([])
    const [oneTask, setoneTask] = useState({})
    


    const getTasks=async()=>{
        try {
            
            const res=await getTasksRequest()
            console.log(res);
            settasks(res.data)
        } catch (error) {
            console.log(error);
            
        }

    }

    const createTask= async task =>{
        try {
            
            const res= await createTaskRequest(task)
            console.log(res);
            console.log(task);
        } catch (error) {
            console.log(error);
            
        }

    }

    const deleteTask =  async (id) =>{
        try {
           const res= await deleteTaskRequest(id)
           console.log(res);
           if (res.status===204) settasks(tasks.filter(task => task.id != id))
            
           
        } catch (error) {
            console.log(res);
            
        }
    }

    const getTask= async (id)=>{
        try {
            const {data}=await getTaskRequest(id)
            console.log(data);
            // setoneTask(data)
            return data
        } catch (error) {
            console.log(error);
            
        }
    }

    const updateTask= async (id,task)=>{
        try {
            
            await updateTaskRequest(id,task)
        } catch (error) {
            console.log(error);
            
        }

    }

    const searchTaskFilter= async ({fecha_inicial,fecha_final,accion,capitulo,municipio,relevante})=>{
        try {

                        
             const res=await filterTaskRequest({fecha_inicial,fecha_final,capitulo,accion,municipio,relevante})
             settasks(res.data)
        } catch (error) {
            console.log(error);
            
        }

    }

    //List Prevencion Tema
    const getListPrevencion=async()=>{
        try {
            
            const res=await getListPrevencionRequest()
            console.log(res);
            return res.data
            // settasks(res.data)
        } catch (error) {
            console.log(error);
            
        }

    }

    const createListPrevencion= async ({actividad_prevencion}) =>{
        try {
            
            const res= await createListPrevencionRequest({nombre_actividad:actividad_prevencion})
            console.log(res);
            console.log(actividad_prevencion);
        } catch (error) {
            console.log(error);
            
        }

    }


    const deleteListPrevencion =  async (id) =>{
        try {
            console.log(id);
           const res= await deleteListPrevencionRequest(id)
           console.log(res);
           //if (res.status===204) settasks(tasks.filter(task => task.id != id))
            
           
        } catch (error) {
            console.log(res);
            
        }
    }


    return(
        <TaskContext.Provider value={{
            tasks,
            createTask,
            getTasks,
            deleteTask,
            getTask,
            oneTask,
            updateTask,
            searchTaskFilter,
            getListPrevencion,
            createListPrevencion,
            deleteListPrevencion

        }}>
            {children}
        </TaskContext.Provider>
    )
}