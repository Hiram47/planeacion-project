import {useForm} from 'react-hook-form'
import { useAuth } from '../context/AuthContext'
import { useTasks } from '../context/TasksContext'
import { useEffect, useRef, useState } from 'react'
import { TablePrevencion } from '../components/TablePrevencion'
import { Toaster, toast } from 'sonner';
import { useNavigate, useParams } from 'react-router-dom'
import { useDownloadExcel } from 'react-export-table-to-excel';




    





export const TaskFormPage = () => {
    const {register,handleSubmit,setValue,getValues,watch,formState:{errors},reset}=useForm({mode:'onBlur'})

    const {
        register: register2,
        formState: { errors: errors2 },
        handleSubmit: handleSubmit2,
      } = useForm({
        mode: "onBlur",
      });


    const {user}=useAuth()
    const {tasks,createTask,getTasks,getTask,updateTask,searchTaskFilter,getListPrevencion}=useTasks()
    const navigate =useNavigate()

    // actualizar tabla
    const [load, setload] = useState(false)

    //lista 
    const [listaTema, setlistaTema] = useState([])


    // setact_sec(getValues('componente'))
    // console.log(act_sec);

    //ver el cambio del combobox
    const watchShowAge = watch("componente", false)
    console.log(watchShowAge);


    const [relevante,actividad]= watch(['relevante','actividad'],false)
    console.log(relevante);
    console.log(actividad);
     

    const params=useParams()
    

    // excel
    const tableRef=useRef(null)

    const { onDownload } =useDownloadExcel({
        currentTableRef:tableRef.current,
        filename:'Prevencion_datos',
        sheet:'Prevencion'
    })


    useEffect(() => {

       async function getTema(){
            const lista=await getListPrevencion()
            setlistaTema(lista)
            console.log(lista);

            
            const comp= getValues('componente')
            // setact_sec(getValues('componente'))
            // console.log(act_sec);
            console.log(comp);
            console.log(getValues('actividad'));

          
        }
        getTema()
        
    }, [])
    
    
    useEffect(() => {
        // setact_sec('s')
        // console.log(act_sec);
        
        const subscription = watch((value, { name, type }) =>
        console.log(value, name, type)
        
      )
      return () => subscription.unsubscribe()
     
    }, [watch])

    //llenar los combos
    let noe=[]

    if (watchShowAge=='C1. Coordinación interinstitucional para la generación de entornos de seguridad y de paz implementada') {
        console.log('hiram');
        // setact_sec('hiram')
        // console.log(act_sec);
        noe=[
            'A1.C1. Fortalecimiento de las capacidades institucionales para la prevención de conductas antisociales',
            'A2.C1 Implementación acciones transversales para la construcción de la paz'
        ]
        console.log(noe);
        
    }else if(watchShowAge=='C2. Entornos sociales seguros'){
        noe=[
            'A1.C2 Conformación de brigadas para la generación de espacios seguros',
            'A2.C2 Realización de acciones de prevención y seguimiento de la violencia contra la mujer',
            'A3.C2 Realización de acciones para la atención de la violencia y la delincuencia',
            'A4.C2 Impulso la cultura de la paz en niños niñas y jóvenes para mejorar el entorno social',
            'A5.C2 Realización de acciones culturales artísticas y recreativas para crear factores de protección a través de distintas disciplinas'
        ]
        console.log(noe);

    }else if(watchShowAge=='C3. Participación ciudadana para el desarrollo de comunidades seguras'){
        noe=[
            'A1.C3 Conformación Consejos de Ciudadanos Seguridad_Municipal',
            'A2.C3 Desarrollo mecanismos para la participación ciudadana'
        ]
        console.log(noe);


    }
    else{
        console.log('no es lo mismo');
        noe=[
            ''
            
        ]
        console.log(noe);

    }
    
    


    





    
    

    const date = new Date();

let day = date.getDate();
let month = date.getMonth() + 1;
let year = date.getFullYear();



    let currentDate=''
if (month<10) {
    currentDate = `${year}-0${month}-${day}`;
    
}if (day<10) {
    currentDate = `${year}-${month}-0${day}`;
    
}
if (day<10 ||month<10) {
    currentDate = `${year}-0${month}-0${day}`;
    
}
else{
    currentDate = `${year}-${month}-${day}`;
    
}
console.log(currentDate); // "17-6-2022"
    
    console.log(tasks);
    console.log(user);

    useEffect(() => {
        getTasks()
        setValue('fecha_registro',currentDate)
         setload(false)
    
    }, [load])



    useEffect(() => {

        async function loadTask(){

            
            
            if (params.id) {
                
                const oneTask= await getTask(params.id)
                console.log(params.id);
            console.log(oneTask);
            setValue('fecha',oneTask.fecha)
            setValue('capitulo',oneTask.capitulo)
            setValue('componente',oneTask.componente)
            setValue('actividad',oneTask.actividad)
            setValue('accion',oneTask.accion)
            setValue('tema',oneTask.tema)
            setValue('descripcion',oneTask.descripcion)
            setValue('factor',oneTask.factor)
            setValue('grupo',oneTask.grupo)
            setValue('mujeres',oneTask.mujeres)
            setValue('hombres',oneTask.hombres)
            setValue('lugar',oneTask.lugar)
            setValue('municipio',oneTask.municipio)
            setValue('colonia',oneTask.colonia)
            setValue('latitud',oneTask.latitud)
            setValue('longitud',oneTask.longitud)
            setValue('alerta_genero',oneTask.alerta_genero)
            setValue('estrategia_vive',oneTask.estrategia_vive)
            setValue('medio_verificacion',oneTask.medio_verificacion)
            setValue('fecha_registro',oneTask.fecha_registro)


            //new
            setValue('descripcion_accion',oneTask.descripcion_accion)
            setValue('acuerdos',oneTask.acuerdos)
            setValue('participantes',oneTask.participantes)
            setValue('observaciones',oneTask.observaciones)
            setValue('relevante',oneTask.relevante)
            
            
        }
    }
    loadTask()
        
    }, [])




    useEffect(() => {

        async function loadTask(){

            
            
            if (params.id) {
                
                const oneTask= await getTask(params.id)
                console.log(params.id);
            console.log(oneTask);
            setValue('fecha',oneTask.fecha)
            setValue('capitulo',oneTask.capitulo)
            setValue('componente',oneTask.componente)
            setValue('actividad',oneTask.actividad)
            setValue('accion',oneTask.accion)
            setValue('tema',oneTask.tema)
            setValue('descripcion',oneTask.descripcion)
            setValue('factor',oneTask.factor)
            setValue('grupo',oneTask.grupo)
            setValue('mujeres',oneTask.mujeres)
            setValue('hombres',oneTask.hombres)
            setValue('lugar',oneTask.lugar)
            setValue('municipio',oneTask.municipio)
            setValue('colonia',oneTask.colonia)
            setValue('latitud',oneTask.latitud)
            setValue('longitud',oneTask.longitud)
            setValue('alerta_genero',oneTask.alerta_genero)
            setValue('estrategia_vive',oneTask.estrategia_vive)
            setValue('medio_verificacion',oneTask.medio_verificacion)
            setValue('fecha_registro',oneTask.fecha_registro)

             //new
             setValue('descripcion_accion',oneTask.descripcion_accion)
             setValue('acuerdos',oneTask.acuerdos)
             setValue('participantes',oneTask.participantes)
             setValue('observaciones',oneTask.observaciones)
             setValue('relevante',oneTask.relevante)

            toast.success('Cambio de datos')
            
            
        }
    }
    loadTask()
        
    }, [params.id])
    

    const onSubmit= handleSubmit(data =>{

        if (params.id) {

            updateTask(params.id,data)

           // createTask(data)
            toast.success('Se ingresaron los datos correctamente')
            setload(true)
           // navigate('/')
            
        } else{
            console.log(data);
            const {fecha,descripcion,capitulo,componente,actividad,latitud,longitud,hombres,mujeres}= data
               if (relevante== false ){

                //    toast.error('Actividad esta vacia')
                //    return ;    
            
            if (fecha==''
                    // ||descripcion==''
                     || actividad==''
                    ||longitud==''||latitud=='') {
                        
                        toast.error('Los campos son obligatorios y/o actividad esta vacio')
            
                    }
                    else{
                        createTask(data)
                        toast.success('Se ingresaron los datos correctamente')
                        setload(true)

                        //RESET FORM
                        reset()
            
                    }
               } 
               else{
                createTask(data)
                        toast.success('Se ingresaron los datos correctamente')
                        setload(true)

                        //RESET FORM
                        reset()
               }

        }

    })

    const onSubmit2= handleSubmit2(  data =>{
        console.log(data);
        searchTaskFilter(data)
        
    })

    console.log(user.area);


    //Permiso de la pagina
    useEffect(() => {
      function PlaneacionPermiso(){
        if (user.area !='planeacion') {
            navigate('/')
            
        }

      }
      PlaneacionPermiso()
    }, [])


   
    

  return (
    
    <div className='bg-zinc-800  w-full p-10 rounded-md' >
        {
            params.id ? (
                <h2 className='text-3xl underline decoration-yellow-500 my-2'>FORMULARIO PREVENCIÓN (EDICIÓN)</h2>

            ):(
                <h2 className='text-3xl underline decoration-cyan-600 my-2'>FORMULARIO PREVENCIÓN</h2>

            )
        }
        {/* <h2 className='text-3xl underline decoration-cyan-600 my-2'>FORMULARIO PREVENCIÓN</h2> */}

        {/* <hr className='my-4' /> */}

        <form key={1} onSubmit={onSubmit}>
        <div className='grid sm:grid-cols-2 md:grid-cols-4 gap-2 '>

            {/* checkbox */}

            <div>
                <label htmlFor="show">Accion relevante: </label>
                <input className='bg-zinc-700 p-2'   type="checkbox" name="" id="show" {...register('relevante')} />
            </div>

<div>


<label htmlFor="">Fecha:</label>
<input  className='w-full bg-zinc-700 text-white px-4 py-2 rounded-md my-2' type="date"  {...register('fecha',{required:true})} autoFocus />
</div>


    {
        !relevante &&
        <>
<div>
        
<label htmlFor="">Capitulo:</label>




<select  className='w-full bg-zinc-700 text-white px-4 py-2 rounded-md my-2'  {...register('capitulo',{required:true})} id="">
    {/* <option >Seleccionar una opcion</option> */}
    <option selected value=""></option>
    <option  value="Prevención del suicidio">Prevención del suicidio</option>
    <option value="Promoción de la participación Ciudadana">Promoción de la participación Ciudadana</option>
    <option value="Prevención en Movimiento">Prevención en Movimiento </option>
    <option value="Cultura de Paz">Cultura de Paz</option>
    <option value="Prevención de la violencia contra la mujer">Prevención de la violencia contra la mujer</option>




</select>
</div>
<div>
    {/* <h2>{getValues('componente')}</h2> */}
    {/* <h3>{watchShowAge}</h3> */}
<label htmlFor="">Componente:</label>
<select className='w-full bg-zinc-700 text-white px-4 py-2 rounded-md my-2'  {...register('componente')} id="">
    {/* <option defaultValue={'1'}>selecionar componente</option> */}
    <option selected value=""></option>
    <option  value="C1. Coordinación interinstitucional para la generación de entornos de seguridad y de paz implementada">C1. Coordinación interinstitucional para la generación de entornos de seguridad y de paz implementada</option>
    <option value="C2. Entornos sociales seguros">C2. Entornos sociales seguros</option>
    <option value="C3. Participación ciudadana para el desarrollo de comunidades seguras">C3. Participación ciudadana para el desarrollo de comunidades seguras</option>





</select>
</div>

<div>


<label htmlFor="">Actividad:</label>
<select className='w-full bg-zinc-700 text-white px-4 py-2 rounded-md my-2'  {...register('actividad')} >
    {/* <option defaultValue={'1'}>seleccionar actividad</option> */}
    {/* <option selected value="A1.C3 Conformación Consejos de Ciudadanos Seguridad Municipal">A1.C3 Conformación Consejos de Ciudadanos Seguridad Municipal</option>
    <option value="A2.C3 Desarrollo mecanismos para la participación ciudadana">A2.C3 Desarrollo mecanismos para la participación ciudadana</option>
    <option value="A2.C3 Desarrollo mecanismos para la participación ciudadana">{bb}</option> */}

    {
        noe.map(n=>(
            <option selected value={n}>{n}</option>
            
        ))
    }
    





</select>
</div>
        </>

    }






<div>

    {
        relevante
        ?
        <>
        <label htmlFor="">Accion:</label>
        <input className='w-full bg-zinc-700 text-white px-4 py-2 rounded-md my-2' type="text"   {...register('accion')} placeholder='Accion'  />
        </>


        :
        <>
        
<label htmlFor="">Accion:</label>
<select className='w-full bg-zinc-700 text-white px-4 py-2 rounded-md my-2'   {...register('accion')} id="">
    {/* <option defaultValue={'1'}>seleccionar accion</option> */}
    <option  value=""></option>
    <option  value="Brigadas">Brigadas</option>
    <option value="Apoyo ciudadano">Apoyo ciudadano</option>
    <option  value="Ferias">Ferias</option>
    <option value="Asesorías">Asesorías</option>
    <option  value="Campañas">Campañas</option>
    <option value="Eventos">Eventos</option>
    <option  value="Convenios">Convenios</option>
    <option value="Conformación">Conformación</option>
    <option value="Platica">Platica</option>
    <option value="Taller">Taller</option>
    <option value="Curso">Curso</option>

    <option value="MUCPAZ">MUCPAZ</option>
    <option value="Difusión">Difusión</option>
    <option value="Comunitaria">Comunitaria</option>
    <option value="Seguimiento">Seguimiento</option>
    <option value="Cine con prevención">Cine con prevención</option>
    <option value="Contención">Contención</option>
    


</select>
        </>
    }

</div>

{/* new */}

{

relevante &&
<>
<div>

    <label htmlFor="">Descripcion accion:</label>
    <input className='w-full bg-zinc-700 text-white px-4 py-2 rounded-md my-2' type="text"   {...register('descripcion_accion')} placeholder='Descripcion accion'  />
</div>

<div>

    <label htmlFor="">Acuerdos:</label>
    <input className='w-full bg-zinc-700 text-white px-4 py-2 rounded-md my-2' type="text"   {...register('acuerdos')} placeholder='Acuerdos'  />
</div>
<div>

    <label htmlFor="">Participantes:</label>
    <input className='w-full bg-zinc-700 text-white px-4 py-2 rounded-md my-2' type="number"   {...register('participantes')} placeholder='Participantes'  />
</div>
<div>

    <label htmlFor="">Observaciones:</label>
    <input className='w-full bg-zinc-700 text-white px-4 py-2 rounded-md my-2' type="text"   {...register('observaciones')} placeholder='Observaciones'  />
</div>
</>
}

{/* end */}

{
    !relevante &&
    <>
    
<div>


<label htmlFor="">Tema:</label>
        <select className='w-full bg-zinc-700 text-white px-4 py-2 rounded-md my-2'  {...register('tema')} id="">
        
            {
                !relevante ?
                listaTema.map(t=>(
                    <option selected value={t.nombre_actividad}>{t.nombre_actividad}</option>
                    ))
                    :
                    <option selected value=''></option>

            }

        </select>
</div>
    </>
}

{
    !relevante
    &&
    <>
    
<div>

    <label htmlFor="">Descripcion:</label>
    <input className='w-full bg-zinc-700 text-white px-4 py-2 rounded-md my-2' type="text"   {...register('descripcion')} placeholder='Descripcion'  />
</div>
</>
}
{
    !relevante?
    <>
<div>

        <label htmlFor="">Factor:</label>
        <select className='w-full bg-zinc-700 text-white px-4 py-2 rounded-md my-2'   {...register('factor')} id="">
            {/* <option defaultValue={'1'}>seleccionar accion</option> */}
            <option  selected value=""></option>
            
            <option  value="Capital social debilitado y participación incipiente">Capital social debilitado y participación incipiente</option>
            <option value="Falta de oportunidades laborales">Falta de oportunidades laborales</option>
            
            <option  value="Entornos de ilegalidad en espacio público">Entornos de ilegalidad en espacio público</option>
            <option value="Espacios públicos para la convivencia insuficientes y deteriorados">Espacios públicos para la convivencia insuficientes y deteriorados</option>

            <option  value="Marginación y exclusión social">Marginación y exclusión social</option>
            <option value="Reproducción de roles y estereotipos que general la violencia de género">Reproducción de roles y estereotipos que general la violencia de género</option>
            
            <option value="Inteligencia emocional debilitada y discriminación">Inteligencia emocional debilitada y discriminación</option>
            <option value="Salud mental">Salud mental</option>

            <option value="Embarazo en la adolescencia">Embarazo en la adolecencia</option>
            <option value="Consumo y abuso de drogas legales e ilegales">Consumo y abuso de drogas legales e ilegales</option>
            
            <option value="Ambiente Familiar deteriorado">Ambiente Familiar deteriorado</option>
            <option value="Desercion escolar">Desercion escolar</option>


        </select>
</div>
</>
:
!relevante &&
<div>

<label htmlFor="">Factor:</label>
<select className='w-full bg-zinc-700 text-white px-4 py-2 rounded-md my-2'   {...register('factor')} id="">
    {/* <option defaultValue={'1'}>seleccionar accion</option> */}
    

    <option selected value=""></option>
    





</select>
</div>

}
{
    !relevante ?

<div>

        <label htmlFor="">Grupo:</label>
        <select className='w-full bg-zinc-700 text-white px-4 py-2 rounded-md my-2' {...register('grupo')} id="">
            {/* <option defaultValue={'1'}>seleccionar accion</option> */}
            {/* <option selected value=""></option>  */}
            <option selected value=""></option> 
            <option  value="Niñas/Niños">Niñas/Niños</option>
            <option value="Adolecentes">Adolecentes</option> 
            <option value="Jóvenes">Jóvenes</option> 
            <option value="Mujeres">Mujeres</option>
            <option value="Hombres">Hombres</option>
            <option value="APL">APL</option>
            <option value="HPL">HPL</option>
            <option value="MPL">MPL</option> 

            {/* //modificacion */}

            <option value="Adultos">Adultos</option> 
            <option value="Niñas/Niños/adolecentes">Niñas/Niños/adolecentes</option>
            <option value="Niñas/Niños/adolecentes/adultos">Niñas/Niños/adolecentes/adultos</option>       
       





        </select>
</div>
:
!relevante&&
<div>

<label htmlFor="">Grupo:</label>
<select className='w-full bg-zinc-700 text-white px-4 py-2 rounded-md my-2' {...register('grupo')} id="">
    {/* <option defaultValue={'1'}>seleccionar accion</option> */}
    {/* <option selected value=""></option>  */}
    <option  selected value=""></option>





</select>
</div>
}

{/* <>

  
        <div>

                <label htmlFor="">Factor:</label>
                <select className='w-full bg-zinc-700 text-white px-4 py-2 rounded-md my-2'   {...register('factor')} id="">
                    
                <option selected value=""></option>
                    





                </select>
        </div>
        
</> */}

{
    !relevante &&
<>





    <div>

    <label htmlFor="">Mujeres:</label>
        <input className='w-full bg-zinc-700 text-white px-4 py-2 rounded-md my-2' type="number"  {...register('mujeres')} placeholder='Numero de mujeres'  />
    </div>

    <div>

    <label htmlFor="">Hombres:</label>
        <input className='w-full bg-zinc-700 text-white px-4 py-2 rounded-md my-2' type="number"  {...register('hombres')} placeholder='Numero de hombres'  />
    </div>

</>
}
{
    !relevante?
    <>

<div>

        <label htmlFor="">Municipio:</label>
        <select className='w-full bg-zinc-700 text-white px-4 py-2 rounded-md my-2'  {...register('municipio')} id="">
            {/* <option defaultValue={'1'}>seleccionar accion</option> */}
            <option value=""></option>
            <option value="Canatlán">Canatlán</option>
                  <option value="Canelas">Canelas</option>
                  <option value="Coneto de Comonfort">Coneto de Comonfort</option>
                  <option value="Cuencamé">Cuencamé</option>
                  <option value="Durango">Durango</option>
                  <option value="General Simón Bolívar">General Simón Bolívar</option>
                  <option value="Gómez Palacio">Gómez Palacio</option>
                  <option value="Guadalupe Victoria">Guadalupe Victoria</option>
                  <option value="Guanaceví">Guanaceví</option>
                  <option value="Hidalgo">Hidalgo</option>
                  <option value="Indé">Indé </option>
                  <option value="Lerdo">Lerdo</option>
                  <option value="Mapimí">Mapimí</option>
                  <option value="Mezquital">Mezquital</option>
                  <option value="Nazas">Nazas</option>
                  <option value="Nombre de Dios ">Nombre de Dios </option>
                  <option value="Ocampo">Ocampo</option>
                  <option value="El Oro">El Oro</option>
                  <option value="Otáez">Otáez </option>
                  <option value="Pánuco de Coronado">Pánuco de Coronado</option>
                  <option value="Peñón Blanco">Peñón Blanco</option>
                  <option value="Poanas">Poanas</option>
                  <option value="Pueblo Nuevo">Pueblo Nuevo</option>
                  <option value="Rodeo">Rodeo </option>
                  <option value="San Bernardo">San Bernardo</option>
                  <option value="San Dimas">San Dimas  </option>
                  <option value="San Juan de Guadalupe">San Juan de Guadalupe</option>
                  <option value="San Juan del Río">San Juan del Río</option>
                  <option value="San Luis del Cordero">San Luis del Cordero</option>
                  <option value="San Pedro del Gallo">San Pedro del Gallo</option>
                  <option value="Santa Clara">Rodeo </option>
                  <option value="Santiago Papasquiaro">Santiago Papasquiaro </option>
                  <option value="Súchil">Súchil </option>
                  <option value="Tamazula">Tamazula</option>
                  <option value="Tepehuanes">Tepehuanes</option>
                  <option value="Tlahualilo">Tlahualilo</option>
                  <option value="Topia">Topia</option>
                  <option value="Vicente Guerrero">Vicente Guerrero  </option>
                  <option value="Nuevo Ideal">Nuevo Ideal</option>     





        </select>
</div>


    </>
    :
    
        !relevante &&

        <div>
        
        <label htmlFor="">Municipio:</label>
        <select className='w-full bg-zinc-700 text-white px-4 py-2 rounded-md my-2'  {...register('municipio')} id="">
        {/* <option defaultValue={'1'}>seleccionar accion</option> */}
        
        <option selected value=""></option>     
        
        
        
        
        
        </select>
        </div>
    

}

{
    !relevante &&

<div>
<label htmlFor="">Colonia o fraccionamiento:</label>
    <input className='w-full bg-zinc-700 text-white px-4 py-2 rounded-md my-2' type="text" {...register('colonia')} placeholder='Colonia o fraccionamiento (Completo)'  />
</div>
}














<div>
<label htmlFor="">Lugar:</label>
    <input className='w-full bg-zinc-700 text-white px-4 py-2 rounded-md my-2' type="text"  {...register('lugar')} placeholder='Lugar'  />
</div>





<div>
<label htmlFor="">Latitud:</label>
    <input className='w-full bg-zinc-700 text-white px-4 py-2 rounded-md my-2' type="text"  {...register('latitud')} placeholder='Latitud'  />
</div>

<div>
<label htmlFor="">Longitud:</label>
    <input className='w-full bg-zinc-700 text-white px-4 py-2 rounded-md my-2' type="text"  {...register('longitud')} placeholder='Longitud'  />
</div>

{
    !relevante&&
    <>

<div>

        <label htmlFor="">Alerta de genero:</label>
        <select className='w-full bg-zinc-700 text-white px-4 py-2 rounded-md my-2'  {...register('alerta_genero')} id="">
            {/* <option defaultValue={'1'}>seleccionar accion</option> */}
            <option selected value=""></option>
            <option  value="SI">SI</option>
            <option value="NO">NO</option>      





        </select>
</div>


<div>

        <label htmlFor="">Estrategia vive:</label>
        <select className='w-full bg-zinc-700 text-white px-4 py-2 rounded-md my-2'  {...register('estrategia_vive')} id="">
            {/* <option defaultValue={'1'}>seleccionar accion</option> */}
            <option selected value=""></option>
            <option value="SI">SI</option>
            <option value="NO">NO</option>    

        </select>
</div>

    </>
}
{
    !relevante ?
<div>

        <label htmlFor="">Medio verificacion:</label>
        <select className='w-full bg-zinc-700 text-white px-4 py-2 rounded-md my-2'  {...register('medio_verificacion')} id="">
            {/* <option defaultValue={'1'}>seleccionar accion</option> */}
            <option selected value=""></option>

            <option  value="Carpeta de evidencias de prevención del suicidio">Carpeta de evidencias de prevención del suicidio</option>
            <option value="Carpeta de evidencias de prevención de violencia contra la mujer">Carpeta de evidencias de prevención de violencia contra la mujer</option>   
            <option value="Carpeta de evidencias de promoción de la participación ciudadana">Carpeta de evidencias de promoción de la participación ciudadana</option>   
            <option value="Carpeta de evidencias de cultura de la paz">Carpeta de evidencias de cultura de la paz</option>   
            <option value="Carpeta de evidencias de prevención en movimiento.">Carpeta de evidencias de prevención en movimiento. </option>  
            <option value="Archivo digital de fotografías">Archivo digital de fotografías</option>   
            <option value="Convenio firmado">Convenio firmado</option> 

            <option value="Acta Constitutiva firmada">Acta Constitutiva firmada</option> 
            <option value="Base de datos de Excel">Base de datos de Excel</option> 
            <option value="Hoja de orientación psicológica ">Hoja de orientación psicológica </option> 
            <option value="Reporte de orientación jurídica ">Reporte de orientación jurídica </option> 





        </select>
</div>
:

<div>

<label htmlFor="">Medio verificacion:</label>
<input className='w-full bg-zinc-700 text-white px-4 py-2 rounded-md my-2' type="text"  {...register('medio_verificacion')} placeholder='Medio verificacion'  />


</div>


}


<div>
{/* <label htmlFor=""  >Fecha registro:</label> */}
    <input className='w-full bg-zinc-700 text-white px-4 py-2 rounded-md my-2' type="hidden"  {...register('fecha_registro')} placeholder='Fecha registro'  />
</div>













        </div>

        {
            params.id ? (
                <button className='w-full py-3 bg-yellow-500 rounded-md my-5' ><i className="fa fa-file-text" aria-hidden="true"></i>
                Editar</button>

            ):(
                <button className='w-full py-3 bg-sky-600 rounded-md my-5' > <i className="fa fa-floppy-o" aria-hidden="true"></i>
                Guardar</button>

            )
        }
</form>


<h2 className='text-2xl font-bold'>BUSQUEDA</h2>
<hr className='py-2'/>

<form key={2} onSubmit={onSubmit2}>
    <div className='grid sm:grid-cols-2 md:grid-cols-4 gap-2' >
    <div>

    <label htmlFor="">Fecha inicial:</label>
    <input  className='w-full bg-zinc-700 text-white px-4 py-2 rounded-md my-2' type="date"  {...register2('fecha_inicial')}  />

</div>
<div>
    <label htmlFor="">Fecha final:</label>
    <input  className='w-full bg-zinc-700 text-white px-4 py-2 rounded-md my-2' type="date"  {...register2('fecha_final')}  />
</div>

<div>


<label htmlFor="">Capitulo:</label>
<select  className='w-full bg-zinc-700 text-white px-4 py-2 rounded-md my-2'  {...register2('capitulo')} id="">
    {/* <option >Seleccionar una opcion</option> */}
    <option selected  value=""></option>
    <option  value="Prevención del suicidio">Prevención del suicidio</option>
    <option value="Promoción de la participación Ciudadana">Promoción de la participación Ciudadana</option>
    <option value="Prevención en Movimiento">Prevención en Movimiento </option>
    <option value="Cultura de Paz">Cultura de Paz</option>
    <option value="Prevención de la violencia contra la mujer">Prevención de la violencia contra la mujer</option>




</select>
</div>


<div>

<label htmlFor="">Accion:</label>
<select className='w-full bg-zinc-700 text-white px-4 py-2 rounded-md my-2'   {...register2('accion')} id="">
    {/* <option defaultValue={'1'}>seleccionar accion</option> */}
    <option selected  value=""></option>
    <option value="Brigadas">Brigadas</option>
    <option value="Apoyo ciudadano">Apoyo ciudadano</option>
    <option  value="Ferias">Ferias</option>
    <option value="Asesorías">Asesorías</option>
    <option  value="Campañas">Campañas</option>
    <option value="Eventos">Eventos</option>
    <option  value="Convenios">Convenios</option>
    <option value="Conformación">Conformación</option>
    <option value="Platica">Platica</option>
    <option value="Taller">Taller</option>
    <option value="Curso">Curso</option>

    <option value="MUCPAZ">MUCPAZ</option>
    <option value="Difusión">Difusión</option>
    <option value="Comunitaria">Comunitaria</option>
    <option value="Seguimiento">Seguimiento</option>
    <option value="Cine con prevención">Cine con prevención</option>
    <option value="Contención">Contención</option>
    


</select>
</div>

<div>

        <label htmlFor="">Municipio:</label>
        <select className='w-full bg-zinc-700 text-white px-4 py-2 rounded-md my-2'  {...register2('municipio')} id="">
            {/* <option defaultValue={'1'}>seleccionar accion</option> */}
                  <option value=""></option>
                  <option value="Canatlán">Canatlán</option>
                  <option value="Canelas">Canelas</option>
                  <option value="Coneto de Comonfort">Coneto de Comonfort</option>
                  <option value="Cuencamé">Cuencamé</option>
                  <option value="Durango">Durango</option>
                  <option value="General Simón Bolívar">General Simón Bolívar</option>
                  <option value="Gómez Palacio">Gómez Palacio</option>
                  <option value="Guadalupe Victoria">Guadalupe Victoria</option>
                  <option value="Guanaceví">Guanaceví</option>
                  <option value="Hidalgo">Hidalgo</option>
                  <option value="Indé">Indé </option>
                  <option value="Lerdo">Lerdo</option>
                  <option value="Mapimí">Mapimí</option>
                  <option value="Mezquital">Mezquital</option>
                  <option value="Nazas">Nazas</option>
                  <option value="Nombre de Dios ">Nombre de Dios </option>
                  <option value="Ocampo">Ocampo</option>
                  <option value="Otáez">Otáez </option>
                  <option value="Pánuco de Coronado">Pánuco de Coronado</option>
                  <option value="Peñón Blanco">Peñón Blanco</option>
                  <option value="Poanas">Poanas</option>
                  <option value="Pueblo Nuevo">Pueblo Nuevo</option>
                  <option value="Rodeo">Rodeo </option>
                  <option value="San Bernardo">San Bernardo</option>
                  <option value="San Dimas">San Dimas  </option>
                  <option value="San Juan de Guadalupe">San Juan de Guadalupe</option>
                  <option value="San Juan del Río">San Juan del Río</option>
                  <option value="San Luis del Cordero">San Luis del Cordero</option>
                  <option value="San Pedro del Gallo">San Pedro del Gallo</option>
                  <option value="Santa Clara">Rodeo </option>
                  <option value="Santiago Papasquiaro">Santiago Papasquiaro </option>
                  <option value="Súchil">Súchil </option>
                  <option value="Tamazula">Tamazula</option>
                  <option value="Tepehuanes">Tepehuanes</option>
                  <option value="Tlahualilo">Tlahualilo</option>
                  <option value="Topia">Topia</option>
                  <option value="Vicente Guerrero">Vicente Guerrero  </option>
                  <option value="Nuevo Ideal">Nuevo Ideal</option>     





        </select>
</div>

{/* new */}

<div>

<label htmlFor="">Relevante:</label>
<select className='w-full bg-zinc-700 text-white px-4 py-2 rounded-md my-2'   {...register2('relevante')} id="">
    {/* <option defaultValue={'1'}>seleccionar accion</option> */}
    <option selected  value=""></option>
    <option value='1'>SI</option>
    
   
    


</select>
</div>


    </div>
        <button className='bg-green-700 p-3 rounded-md w-full my-5  ' > Buscar registros</button>


</form>





{/* tabla */}
<h2 className='text-3xl' >DATOS DE PREVENCIÓN </h2>
<hr  className='py-3'/>
<button className='bg-green-800 py-3 px-3 my-2 rounded-sm ' onClick={onDownload} ><i className="fa fa-file-excel-o" aria-hidden="true"></i>
  Exportar</button>

<div className='relative overflow-x-auto overflow-y-scroll h-lvh  shadow-md sm:rounded-lg'>
    


<table ref={tableRef}  className= 'w-full text-sm text-left text-white dark:text-white '
// className= ' bg-zinc-800 max-w-md w-full p-10 rounded-lg    min-w-full text-center text-md font-light  '
>
    <thead className='text-xs text-white uppercase bg-sky-700 dark:bg-sky-700 dark:text-white-400 dark:text-white-400'
    // className='border-b bg-red-900 font-medium dark:border-neutral-500 dark:text-white'
    >
    <tr>
      <th 
      scope="col" className='px-6 py-3'
      // className='col-md-1'
      >#</th>

      <th scope="col" className='px-6 py-3'>Fecha</th>
      <th scope="col" className='px-6 py-3'>Capitulo</th>
      <th scope="col" className='px-6 py-3'>Componente</th>
      <th scope="col" className='px-6 py-3'>Actividad</th>
      <th scope="col" className='px-6 py-3'>Accion</th>

      <th scope="col" className='px-6 py-3'>Tema</th>
      <th scope="col" className='px-6 py-3'>Descripcion</th>
      <th scope="col" className='px-6 py-3'>Factor</th>
      <th scope="col" className='px-6 py-3'>Grupo</th>
      <th scope="col" className='px-6 py-3'>Mujeres</th>
      <th scope="col" className='px-6 py-3'>Hombres</th>
      <th scope="col" className='px-6 py-3'>Lugar</th>
      <th scope="col" className='px-6 py-3'>Municipio</th>
      <th scope="col" className='px-6 py-3'>Colonia</th>
      <th scope="col" className='px-6 py-3'>Latitud</th>
      <th scope="col" className='px-6 py-3'>Longitud</th>
      <th scope="col" className='px-6 py-3'>Alerta de genero</th>
      <th scope="col" className='px-6 py-3'>Estrategia vive</th>
      <th scope="col" className='px-6 py-3'>Medio verificacion</th>

      <th scope="col" className='px-6 py-3'>Descripcion accion</th>
      <th scope="col" className='px-6 py-3'>Acuerdos</th>
      <th scope="col" className='px-6 py-3'>Participantes</th>
      <th scope="col" className='px-6 py-3'>Observaciones</th>
      <th scope="col" className='px-6 py-3'>relevante</th>

      <th scope="col" className='px-6 py-3'>Eliminar</th>
      <th scope="col" className='px-6 py-3'>Editar</th>
      {/* <th scope="col" className='px-6 py-3'>Cliente</th>
      <th scope="col" className='px-6 py-3'>Color</th>
      <th scope="col" className='px-6 py-3'>Costo</th>
      <th scope="col" className='px-6 py-3'>Venta</th> */}
      {/* <th className='col-md-1'>Serie</th>
      <th className='col-md-1'>Placas</th> 
    <th className='col-md-1'>Inventario</th> */}
      {/* <th> Accion</th> */}
      {/* <th>Reporte</th> */}
    </tr>

    </thead>
    <tbody>

        {

            tasks.length ===0 ? <h2>No hay registros</h2>
            :
            tasks.map((task,i)=>(
                <TablePrevencion task={task} role={user.role} area={user.area} i={i} key={task.id}/>
            ))
            

        }

         {/* {
            Gastos.map((gasto,i)=>(

                <TableGastos gasto={gasto} i={i} key={gasto.id} totald={anidado+=gasto.cantidad} />
              //  setfech={gasto.}

            ))
} */}
   

    </tbody>

</table>








    </div>
{/* new */}



{/* end */}
    <Toaster richColors   position="top-center" expand={true} />

        

    </div>
  )
}
