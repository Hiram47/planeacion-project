import { useForm } from "react-hook-form"
import { useAuth } from "../context/AuthContext"
import { Link, useNavigate } from "react-router-dom"
import { useEffect } from "react"




export const LoginPage = () => {

    const {register,handleSubmit,formState:{errors}}=useForm()
    const {signin,errors:signinErrors ,isAuthenticated}=useAuth()
    const navigate=useNavigate()

    const onSubmit= handleSubmit(data=>{
        console.log(data);
        signin(data)
    })

    useEffect(() => {
        if (isAuthenticated)  navigate('/tasks')
        
    }, [isAuthenticated])
    



  return (
    <div className="flex h-[calc(100vh-100px)] items-center justify-center">
        <div className="bg-zinc-800 max-w-md w-full p-10 rounded-md">
            <h1 className="text-2xl font-bold py-2" >Login</h1>
            {
            signinErrors.map((error,i)=> {
                return <div key={i+1} className='bg-red-500 p-2 m-2 text-white'>
                    {error}
                </div>
            })
        }
        <form onSubmit={onSubmit}>
     

     <input placeholder='Email' type="email" name="email" {...register('email',{required:true})}  className='w-full bg-zinc-700 text-white px-4 py-2 my-2 rounded-md'/>
     {
         errors.email&& (
             <p className='text-red-500' >Email es obligatorio</p>
         )
     }

     <input placeholder='Contrasena' type="password" name="password" {...register('password',{required:true})} className='w-full bg-zinc-700 text-white px-4 py-2 my-2 rounded-md' />
     {
         errors.password&& (
             <p className='text-red-500' >Contrasena es obligatoria</p>
         )
     }



         <button className="bg-sky-700 py-1 px-2 rounded-md">login</button>
 </form>

 {/* <p className="flex gap-x-2 justify-between">
    Registrate <Link to={'/register'} className="text-sky-500  ">Registrarse</Link>
 </p> */}



        </div>
      
    </div>
  )
}
