import React, { useEffect, useState } from 'react'
import { useForm } from 'react-hook-form'
import {Toaster,toast} from 'sonner'
import { Tabs } from 'flowbite-react';
import { HiAdjustments, HiClipboardList, HiUserCircle } from 'react-icons/hi';
import { MdDashboard } from 'react-icons/md';
import { useTasks } from '../context/TasksContext';
import { useNavigate } from 'react-router-dom';
import { useAuth } from '../context/AuthContext';


export const AdminPage = () => {
    const {register,handleSubmit,setValue,formState:{errors}}=useForm({mode:'onBlur'})
    const navigate =useNavigate()

    const [listaTema, setlistaTema] = useState([])
    const { getListPrevencion,createListPrevencion,deleteListPrevencion}=useTasks()
    const {user}=useAuth()


    useEffect(() => {
      function PlaneacionPermiso(){
        if (user.role !=0) {
            navigate('/')
            
        }

      }
      PlaneacionPermiso()
    }, [])



    useEffect(() => {

        async function getTema(){
             const lista=await getListPrevencion()
             setlistaTema(lista)
             console.log(lista);
 
         }
         getTema()
      
     }, [])

     //Role admin
     useEffect(() => {
        function PlaneacionPermiso(){
          if (user.role !=0) {
           navigate('/')
              
          }
  
        }
        PlaneacionPermiso()
      }, [])


    const onSubmit=handleSubmit( data=>{
        console.log(data);
        createListPrevencion(data)
        navigate('/')
    })

    const onDelete =async(id)=>{
        // const res=await deleteListPrevencion(id)
        // console.log(res);
        // toast('Eliminacion Exitosa')
        // navigate('/')
        toast('Estas seguro de eliminar el elemento?',{
            action:{
                label:'Aceptar',
                onClick: async()=>{
                    const res=await deleteListPrevencion(id)
                    navigate('/')
                      console.log(id);
                      console.log(res);
                    console.log(id);
                }
            },
            cancel: {
                label: 'Cancelar',
                onClick: () => console.log('Cancel!'),
              },
          })
       console.log(id);

    }
  return (
    <div className='bg-zinc-800  w-full p-10 rounded-md' >
        <Tabs aria-label="Full width tabs" style="default">
        <Tabs.Item active title="Prevencion" icon={HiUserCircle}>
                <h2 className='text-3xl'>PREVENCIÓN</h2>
                <hr />
                <form key={1} onSubmit={onSubmit}>
                    <div className='' >
                        
                            <label htmlFor="">Tema: </label>
                            <input  className='w-full bg-zinc-700 text-white px-4 py-2 rounded-md my-2' type="text"  {...register('actividad_prevencion',{required:true})} autoFocus />


                        

                    </div>
                        <div>
                            <button className='w-full bg-sky-700 hover:bg-sky-600 py-2 rounded-md'>Guardar</button>
                        </div>
                </form> 

                <div className='grid sm:grid-cols-2 md:grid-cols-5 gap-2 p-4 '>
                {
                    
                    listaTema.map((lst,i)=>(

                        <div key={lst.id} id="toast-top-left" className=" gap-x-2 items-center justify-between flex w-full max-w-xs p-4 space-x-4 text-white bg-slate-800 divide-x rtl:divide-x-reverse divide-gray-200 rounded-lg shadow top-5 left-5 dark:text-gray-400 dark:divide-gray-700 space-x dark:bg-gray-800" role="alert">
                            <div  className="text-sm font-bold">{i+1}._{ lst.nombre_actividad} <h2 onClick={()=>onDelete(lst.id)} className='text-red-700 font-bold text-2xl cursor-pointer'>X</h2></div>
                            
                        </div>
                        // <h2>{lst.nombre_actividad}</h2>
                    ))
                }
                </div>
      </Tabs.Item>
      <Tabs.Item title="Policia" icon={MdDashboard}>
        <>

          <h2>Programando...</h2>
          
        </>

      </Tabs.Item>

        </Tabs>
        {/* */}

        <Toaster richColors   position="top-center" expand={true} />

    </div>
  )
}
