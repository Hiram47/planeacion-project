import {useForm} from 'react-hook-form'
import { useAuth } from '../context/AuthContext'
import { Link, useNavigate } from 'react-router-dom'
import { useEffect } from 'react'



export const RegisterPage = () => {
    const{register,handleSubmit,formState:{errors}}=useForm()
    const {user,signup,isAuthenticated,errors:registerErrors}=useAuth()
    const navigate=useNavigate()

    console.log(user);

    useEffect(() => {
        if(isAuthenticated) navigate('/tasks')
      
    }, [isAuthenticated])
    

    const onSubmit=handleSubmit(async(values)=>{ 
        console.log(values);
        signup(values)
        
          })


  return (
    <div className='flex h-[calc(100vh-100px)] items-center justify-center'>
        <div className='bg-zinc-800 max-w-md p-10 rounded-md'>
            <h2 className='text-2xl font-bold py-2'> Registro</h2>
        {
            registerErrors.map((error,i)=> {
                return <div key={i+1} className='bg-red-500 p-2 text-white'>
                    {error}
                </div>
            })
        }
        <form onSubmit={onSubmit}>
            <input placeholder='Nombre de usuario' type="text" {...register('username',{required:true})} className='w-full bg-zinc-700 text-white px-4 py-2 my-2 rounded-md' />
            {
                errors.username&& (
                    <p className='text-red-500' >Usuario es obligatorio</p>
                )
            }

            <input placeholder='Email' type="email" name="email" {...register('email',{required:true})}  className='w-full bg-zinc-700 text-white px-4 py-2 my-2 rounded-md'/>
            {
                errors.email&& (
                    <p className='text-red-500' >Email es obligatorio</p>
                )
            }

            <input placeholder='Contrasena' type="password" name="password" {...register('password',{required:true})} className='w-full bg-zinc-700 text-white px-4 py-2 my-2 rounded-md' />
            {
                errors.password&& (
                    <p className='text-red-500' >Contrasena es obligatoria</p>
                )
            }

            <label htmlFor="underline_select" className="sr-only">Role</label>
                <select {...register('role')} id="underline_select" className="block py-2.5 my-2 px-0 w-full text-sm text-gray-500 bg-transparent border-0 border-b-2 border-gray-200 appearance-none dark:text-gray-400 dark:border-gray-700 focus:outline-none focus:ring-0 focus:border-gray-200 peer">
                    <option defaultValue={'1'}>Tipo de role</option>
                    <option value="0">ADMIN</option>
                    <option value="1">CAPTURA</option>
                   
                </select>

                {
                errors.role&& (
                    <p className='text-red-500' >Usuario es obligatorio</p>
                )
            }

                <label htmlFor="underline_select" className="sr-only">AREA</label>
                <select {...register('area')} id="underline_select" className="block py-2.5 my-2 px-0 w-full text-sm text-gray-500 bg-transparent border-0 border-b-2 border-gray-200 appearance-none dark:text-gray-400 dark:border-gray-700 focus:outline-none focus:ring-0 focus:border-gray-200 peer">
                    <option  defaultValue={'planeacion'}>Selecciona Area</option>
                    <option value="General">General</option>
                    <option value="planeacion">Planeacion</option>
                   
                </select>
                {
                errors.area&& (
                    <p className='text-red-500' >Usuario es obligatorio</p>
                )
            }

                <button className='bg-sky-700 py-1 px-2 rounded-md'>Registro</button>
        </form>

        <p className="flex gap-x-2 justify-between">
    ya tienes una cuenta? <Link to={'/login'} className="text-sky-500 ">Logearte</Link>
 </p>
    </div>
    </div>


    
  )
}
