// import axios from 'axios'
import axios from './axios'

// const API= 'http://10.11.60.124:3090/api'

export const registerRequest=user => axios.post(`/register`,user)

export const loginRequest = user => axios.post(`/login`,user)

export const verifyTokenRequest= () => axios.get('/verify')