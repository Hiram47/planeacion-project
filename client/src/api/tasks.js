import axios from './axios'

export const getTasksRequest= ()=> axios.get('/tasks')

export const getTaskRequest=(id)=>axios.get(`/tasks/${id}`)

export const createTaskRequest=(task)=>axios.post('/tasks',task)

export const updateTaskRequest=(id,task)=>axios.put(`/tasks/${id}`,task)

export const deleteTaskRequest= (id) => axios.delete(`/tasks/${id}`)


// busqueda
export const filterTaskRequest=(task)=>axios.post('/tasks/filtro',task)

//llenar lista combo
export const getListPrevencionRequest= ()=> axios.get('/temaPrevencion')

export const createListPrevencionRequest= (task)=> axios.post('/temaPrevencion',task)

export const deleteListPrevencionRequest= (id) => axios.delete(`/temaPrevencion/${id}`)