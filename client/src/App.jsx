import {BrowserRouter,Route,Routes} from 'react-router-dom'
import { LoginPage } from './pages/LoginPage'
import { RegisterPage } from './pages/RegisterPage'
import { AuthProvider } from './context/AuthContext'
import { HomePage } from './pages/HomePage'
import { TaskPage } from './pages/TaskPage'
import { TaskFormPage } from './pages/TaskFormPage'
import { ProfilePage } from './pages/ProfilePage'
import { ProtectedRoute } from './ProtectedRoute'
import { TaskProvider } from './context/TasksContext'
import { Navbar } from './components/Navbar'
import { AdminPage } from './pages/AdminPage'

const App = () => {
  return (
    <AuthProvider>
      <TaskProvider>


    <BrowserRouter >
      <Navbar/>
      <Routes>
        <Route path='/' element={<HomePage/>}/>
        <Route path='/login' element={<LoginPage/>}/>
        <Route path='/register' element={<RegisterPage/>}/>

        <Route element={<ProtectedRoute/>}>
          
          <Route path='/tasks' element={<TaskPage/>}/>
          <Route path='/add-task' element={<TaskFormPage/>}/>
          <Route path='/tasks/:id' element={<TaskFormPage/>}/>
          <Route path='/profile' element={<ProfilePage/>}/>


          <Route path='/admin' element={<AdminPage/>}/>

        </Route>
      </Routes>
    </BrowserRouter>
      </TaskProvider>
    </AuthProvider>
  )
}

export default App